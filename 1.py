import os
import uuid
import json
import requests
import time
import dateutil.relativedelta
from datetime import datetime
from flask_restful import Resource
from flask import request, Response, make_response
from werkzeug.exceptions import BadRequest

from cassandra.cqlengine import connection

from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS


app = Flask(__name__)
db = SQLAlchemy(app)


CORS(app, resources={r"/api/*": {"origins": "*"}})


from storage.models import Messages
from models import Chats, association_user_chat


class CreateChatApiHandler(Resource):
    @login_required
    def post(self, *args, **kwargs):
        # create chat
        pass

class ChatApiHandler(Resource):

    @login_required
    def get(self, *args, **kwargs):
        # get chats for user

        pass

class ChatDetailApiHandler(Resource):

    @login_required
    def get(self, *args, **kwargs):
        # get all users from current chat

        pass

    @login_required
    def put(self, *args, **kwargs):
        # change title
        pass

    @login_required
    def head(self, *args, **kwargs):
        # confirm exists chat

        pass


    @login_required
    def delete(self, *args, **kwargs):
        # delete chat
        pass


class ChatModificationApiHandler(Resource):

    @login_required
    def post(self, *args, **kwargs):
        #add user to chat

        return Response(status=201) # id users + id chat

    @login_required
    def delete(self, *args, **kwargs):
        #delete user from chat

        return Response(status=204)


class MessageApiHandler(Resource):

    pass


class MessageDetailApiHandler(Resource):

    @login_required
    @cassandra_connection
    def get(self, *args, **kwargs):
        # get message by id

        return Response(status=404)

    @login_required
    @cassandra_connection
    def put(self, *args, **kwargs):
        # edit message
        pass

    @login_required
    @cassandra_connection
    def delete(self, *args, **kwargs):
        # delete message
        return Response(status=204)


